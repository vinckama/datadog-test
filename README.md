# Website availability & performance monitoring
## Take-home project - DataDog - Vincent Roy 

### Overview
The idea of that console program is to monitor performance and availability of websites.
I chose to do it as a Python package.

**Make sure to have Python 3.7 or later install on your computer.**


### Program installation
To launch the program, you just have to go to the current folder of the 
folder with the command `cd \path\to\folder`.


Now you have two options:
 - Run the `app.sh` script.
 - Install the package in python directly.
 
#### Launch with `app.sh`
 You can execute the following script in the project directory.
 ```BASH
chmod +x ./app.sh
./app.sh
 ```
At any time, you can quit the program with the command `CRTL +C`.

#### Install Package
You have to run the `setup.py` file with python3
You can execute the following script in the project directory.
 ```BASH
python3 setup.py install --user
 ```
You will then be able to run the package from python3.
In your python3 Console:

```python
import website_check
website_check.Manager()
```

The only accessible class is the `Manager` class . 
Two arguments can be added: `loglevel` and `config`.
- loglevel: level of logs, info by default (choice in [CRITICAL, ERROR, WARNING, INFO, DEBUG])
- config: custom location of the config file, see below for default location

#### Configuration file
When the program is run for the first time, a `Vince_DataDog_project` directory at the user root will
 be created and a `config.yml` file will be added to it. 
Two examples will be defined, you can modify this `config.yml` file yourself. 

In that file, you can configure many parameters:
 - Name of the website
 - Url of the website
 - Time between two checks
 - Metrics
 - Alert threshold
 
A specific documentation is located in the configuration file.


### Stats
- Every 10s, display the stats for the past 10 minutes for each website
- Every minute, displays the stats for the past hour for each website


### Alerting
- When a website availability is below 80% for the past 2 minutes, an alert will be raised.
- When availability resumes for the past 2 minutes, the alert will be recovered.


### Tests
The tests for logic alteration can be found in the `./tests/test_alert.py` 
Other tests have been implemented. However, I chose not to test the `Manager` class and some metrics.


### Improvement
I estimate my work at about 12 hours. It took me a little longer because I trained on
 certain aspects:
 - setting up an IC pipeline with Gitlab...
 - use of pytest (I used to use it before unittest)
 - transformation of the program into a package (with setuptools)
 
 
I see three areas for improvement:
- Tests
- User interface
- Metrics

#### Improved testing
As I was saying in the paragraph on Testing. I chose not to test the `Manager` class and some metrics.
I justify this choice because it was not mandatory to test the whole program. Nevertheless, to make it easier for me 
development, I chose to add a number of tests that I felt were relevant. As I ended up
 coding `main.py`, I made the decision not to test this function automatically. I've done several tests
  that I didn't code. 
  
  To make this app more durable, we should add automatic tests 
of this file.

#### User interface improvement
 Staying with the console, I could have imagined a function cleaning up old messages on statistics and
  adding the new statistics instead.
 I preferred to add logs to each new stats display. The user experience is slightly 
 degraded, but I think it's still viable for this type of project.
 
 By imagining a program a little easier to read, we could create an interface on a web page,
 like the DataDog agent which has a command line interface and also a GUI.
 
 Finally to improve the user experience, I could add a way to modify the checked websites directly.
  from the program and not from a configuration file.
 
 
#### Metrics improvement
 I only implemented four metrics. We could imagine adding others in the future like page loading time, login time, ...
 
### Annexe - config.yml example
```yaml
#### configuration file for the website check agent ####
### Take-home project - DataDog - Vincent Roy ###

instances: # DO NOT REMOVE

  # check here the list of websites to be checked
  # two information are mandatory: 'name' and 'url'
  ### EXAMPLE ###
  # - name: ExampleDown
  #   url: http://example.url
  ## optional arguments
  #   metrics: #| Availability is set always by default,
  #            #| you have the choice in
  #      - availability #| availability of the website
  #      - max_response_time #| max of response time metric
  #      - average_response_time #| average of response time metric
  #      - response_codes_count #| count and sort the number of status answer (ex: '1XX', '2XX' ..)
  #   term: 1 #|number of seconds between two checks, 1 by default
  #   memory_time: 5400 #|duration in seconds of the data stored about the website,
  #                        #| I recommend more than 1 hour, 1h30 by default
  #   availability_threshold: 80 #| threshold for the availability alert, 80% by default
  - name: ExampleDown
    url: http://example.url
    term: 2
    memory_time: 3600
    availability_threshold: 10
    metrics:
      - response_codes_count

  - name: ExampleUp
    url: http://example.com
    metrics:
      - availability
      - max_response_time
      - average_response_time
      - response_codes_count


```
