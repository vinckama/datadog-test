#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Vincent Roy
@author: Vincent Roy
"""
import time


class AvailabilityAlert:
    def __init__(self, log, url, metric, threshold, duration, term):
        """    Alert class on availability metric

        According to a certain threshold, inform the user that a website is unavailable
        :param log:  a logger formatter from logging library
        :param url: url of the website
        :param metric: availability metric endpoint
        :param threshold: threshold to alert
        :param duration: duration to compute the average availability
        :param term: number of seconds between two new values
        """
        self.log = log
        self.url = url
        self.threshold = threshold
        self.n_last = int(duration / term)
        if self.n_last <= 0:
            self.log.warning(f"term of {self.url} is too big, "
                             f"sets the number of data points for the calculation of the alert to 1")
            self.n_last = 1
        self.state = False
        self.time_status = None
        self.metric = metric

    def alert(self, availability):
        """send an alert message

        :param availability: the level of real availability associate to the alert
        :return:
        """
        self.state = True
        self.time_status = time.strftime("%Y-%m-%d %H:%M:%S %Z")
        self.log.info(f"{self.url} is DOWN, availability={availability:0.1f}%; time={self.time_status}")

    def recovery(self, availability):
        """send an recovery message

        :param availability: the level of real availability associate to the alert
        :return:
        """
        self.state = False
        self.time_status = time.strftime("%Y-%m-%d %H:%M:%S %Z")
        self.log.info(f"{self.url} is UP, availability={availability:0.1f}%; time={self.time_status}")

    def check(self):
        """method to generate alert and recovery

        :return:
        """
        metric_info = self.metric(self.n_last)
        if metric_info['data_length'] >= self.n_last:
            availability = metric_info['value']
            if availability < self.threshold and not self.state:
                self.alert(availability)
            elif availability >= self.threshold and self.state:
                self.recovery(availability)

            return availability
        return None

    def __call__(self):
        """ return the state of the alert

        :return: dict with info on the alert
        """
        return {
            'availability': self.check(),
            'status': self.state,
            'data_length': self.n_last,
            'status_time': self.time_status
        }
