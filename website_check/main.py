#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Vincent Roy
@author: Vincent Roy
"""
from .agent_check import WebsiteCheck
from .log import init_logging
from os.path import expanduser, join, dirname, exists
import signal
import time
import yaml
import shutil
from .utils import message_duration, recur_dict_message
import sys
import os


class ServiceExit(Exception):
    """
    Custom exception which is used to trigger the clean exit
    of all running threads and the main program.
    """
    pass


class Manager(object):
    HOME = expanduser("~")

    def __init__(self, **kwargs):
        """main class of the project, launch the app

        possible kwargs parameters
        loglevel: level of logs, info by default
        choice in the list of [CRITICAL, ERROR, WARNING, INFO, DEBUG]
        config: location of the config file
        """

        self.log = init_logging(level = kwargs.get('loglevel', ''))
        self.config_file = kwargs.get('config', '')
        self.message_header()
        config_dict = self._open_config()
        sys.stdout.write('\n')
        sys.stdout.flush()

        signal.signal(signal.SIGTERM, self.service_shutdown)
        signal.signal(signal.SIGINT, self.service_shutdown)

        try:
            check_instances = config_dict['instances']
        except (KeyError, AttributeError, TypeError):
            self.log.error(f'Wrong format in the config file (check out the README.md) \n({self.config_file})')
            self.log.info("Shutdown of the app")
            sys.exit()
        if not check_instances:
            self.log.error(f'No website to check in the config file (check out the README.md)\n({self.config_file})')
            self.log.info("Shutdown of the app")
            sys.exit()

        # launch website check agent.
        self.website_checks = {}
        for website in check_instances:
            self.log.debug(website)
            self.website_checks[website['name']] = WebsiteCheck(log = self.log, config = website)

        sys.stdout.write('\n')
        sys.stdout.flush()
        self.run()

    def _open_config(self):
        """open the config file

        manage custom folder for the config file
        :return:
        """
        if self.config_file:
            # if custom location is defined, check that the location exists
            if not os.path.exists(dirname(self.config_file)):
                self.log.error(f'FileNotFoundError({self.config_file})\nFolder of the config file not found, '
                               'check your --config argument or remove it\n')
                self.log.info("Can't solve the issue, shutdown of the app")
                sys.exit()
        else:
            # otherwise, set the location of the config file.
            self.config_file = join(self.HOME, 'Vince_DataDog_project/config.yml')
            project_home = dirname(self.config_file)
            if not exists(project_home):
                os.mkdir(project_home)
                self.log.info("Creating project home directory")

        sys.stdout.write(f'{self.config_file} is the path of the config file\n')
        sys.stdout.flush()

        # get information from the config file
        try:
            config_dict = yaml.load(open(self.config_file), Loader = yaml.FullLoader)
        except FileNotFoundError:
            self.log.info('Config.yml not found, creating config file')
            shutil.copy(join(os.path.dirname(__file__), 'config_doc.yml'), self.config_file)
            config_dict = yaml.load(open(self.config_file), Loader = yaml.FullLoader)
        return config_dict

    @staticmethod
    def service_shutdown(signum, _):
        raise ServiceExit

    def shutdown(self):
        """Terminate the running threads

        Set the shutdown flag on each thread to trigger a clean
        shutdown of each thread
        """
        self.log.info("Shutdown of the app")
        for check in self.website_checks.values():
            check.shutdown_flag.set()

        # Wait for the threads to close...
        for check in self.website_checks.values():
            check.join()

    def run(self):
        """ thread routine

        print website statistics every 10s and every minute
        :return:
        """

        #
        clock_min = time.time()
        clock_h = time.time()
        try:

            # Keep the main thread running, otherwise signals are ignored.
            while True:
                time.sleep(0.5)
                now = time.time()
                if now - clock_h > 60:
                    # print stats of the last 10 minutes every 10s
                    self.get_stats(3600)
                    clock_h = now
                elif now - clock_min > 10:
                    # print stats of the last hour every minute
                    self.get_stats(60 * 10)
                    clock_min = now

        except ServiceExit:
            self.shutdown()

    def get_stats(self, duration=None):
        """method for giving statistics on different websites

        :param duration: time over which the metrics are calculated
        :return:
        """
        for name, check in self.website_checks.items():
            info_dict = check(duration=duration)
            duration_value = min(info_dict['data_length'] * info_dict['term'], duration)

            message = [f"{name} summary (last {message_duration(duration_value)}) \n"]
            message.extend(self.message_alert(info_dict['alert'], info_dict['term'], info_dict['url']))
            message.extend(self.message_metrics(info_dict['metrics']))

            message = ''.join(message)
            self.log.info(message)

    @staticmethod
    def message_alert(alert_dict, term, url):
        """write the "alert" part of the statistics message for one website

        :param alert_dict: dict with alert information
        :param term: number of seconds between two check of the website
        :param url: website url
        :return:
        """
        message = []
        if alert_dict['status']:
            alert_time = message_duration(alert_dict['data_length'] * term)
            message.append(f"ALERT ({alert_dict['status_time']}), {url} is DOWN, ")
            message.append(f"availability={alert_dict['availability']: 0.1f}%  time={alert_time})\n")
        return message

    @staticmethod
    def message_metrics(metric_dict):
        """write the "metric" part of the statistics message for one website

        :param metric_dict: dict with metric information
        :return:
        """
        return recur_dict_message(metric_dict)

    @staticmethod
    def message_header():
        """write and print the header message of the app

        :return:
        """
        try:
            columns, _ = os.get_terminal_size(0)
        except OSError:
            columns = 80
        message = []
        message.append('WEBSITE CHECK AGENT'.center(columns, ))
        message.append(' Take-home project - DataDog - Vincent Roy '.center(columns, '-'))
        message.append(f'To quit the program, use CTRL+C or kill -2 {os.getpid()}\n')
        message = '\n'.join(message)
        sys.stdout.write(message)
        sys.stdout.flush()







