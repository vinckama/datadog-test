#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Vincent Roy
@author: Vincent Roy
"""
import logging
from website_check.utils import call_one

LOG_LEVEL_MAP = {
    'CRITICAL': logging.CRITICAL,
    'ERROR': logging.ERROR,
    'WARNING': logging.WARNING,
    'INFO': logging.INFO,
    'DEBUG': logging.DEBUG,
}


def _get_loglevel(level):
    """Map log levels to strings

    :param level: level of logs, info by default
        choice in the list of [CRITICAL, ERROR, WARNING, INFO, DEBUG]
    :return:
    """
    try:
        level = level.upper()
    except AttributeError:
        level = ''
    return LOG_LEVEL_MAP.get(level, logging.INFO)


@call_one
def init_logging(level=''):
    """Initialize logging

    :param level: level of logs, info by default
        choice in the list of [CRITICAL, ERROR, WARNING, INFO, DEBUG]
    :return:
    """

    # Capture warnings as logs
    logging.captureWarnings(True)
    logger = logging.getLogger('WEB CHECK')
    logger.setLevel(_get_loglevel(level))
    # create formatter
    handler = logging.StreamHandler()
    handler.setLevel(_get_loglevel(level))
    formatter = logging.Formatter(fmt='%(asctime)s | %(name)s | %(levelname)s | %(message)s',
                                  datefmt="%Y-%m-%d %H:%M:%S %Z")
    # add formatter to handler
    handler.setFormatter(formatter)
    # add handler to logger
    logger.addHandler(handler)
    return logger
