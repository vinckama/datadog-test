#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Vincent Roy
@author: Vincent Roy
"""


def message_duration(duration_value):
    """ transforms a duration in seconds into a more readable format

    :param duration_value: int
    :return: str
    """
    if duration_value < 60:
        duration_name = f"{duration_value}s"
    elif 60 <= duration_value < 180:
        duration_name = "min"
    elif 180 <= duration_value < 3600:
        duration_name = f"{int(duration_value // 60)} min"
    elif 3600 <= duration_value < 7200:
        duration_name = "hour"
    else:
        duration_name = f"{int(duration_value // 3600)} h"

    return duration_name


def recur_dict_message(dic, level=0, max_len=30):
    """recursive function to display dict

    :param dic: dict to display
    :param level: do not change unless you want a space offset
    :param max_len: maximum length between keys and dict values
    :return:
    """
    message = []
    indent = "    " * level
    end_len = max(0, max_len - 4*level)
    for name, value in dic.items():
        if isinstance(value, dict):
            message.append(f"{indent}{name}\n")
            message.extend(recur_dict_message(value, level=level + 2))
        elif value is not None:
            message.append(f"{indent}{name: <{end_len}} {value:4.0f}\n")
    return message


def call_one(func):
    """limits the call to a single function only one time"""
    def helper(*args, **kwargs):
        if not helper.called:
            helper.first_return = func(*args, **kwargs)
            helper.called = True
        return helper.first_return
    helper.called = False
    helper.first_return = None
    return helper
