#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Vincent Roy
@author: Vincent Roy
"""


class MetricBase(object):

    def __init__(self, logger, data_length):
        """Mother class of all metrics

        :param logger: a logger formatter from logging library
        :param data_length:  max number of stored data in cache
        """
        self.log = logger
        self.data_length = int(data_length)
        if self.data_length <= 0:
            self.log.error(f'{ValueError}data length < 0')
            raise ValueError

        self.data = []
        self.quality = []  # store the quality of the data, ptu False if the data is irrelevant, True on the contrary

    def _update_data(self, new_item, quality):
        """general class to update the data

        check also the quality of the new item, verify that there
        is no overflow of saved data
        :param new_item: the new item to add
        :param quality: the quality of the new item, False if it's irrelevant, True on the contrary
        :return:
        """
        if len(self.data) == self.data_length:
            self.data.pop(0)
            self.quality.pop(0)
        self.data.append(new_item)
        self.quality.append(quality)

    def __call__(self, n_last=None):
        """ get the metric value for stored items

        :param n_last: the number of item  on which you want get the metric, all item stored by default
        :return:
        """
        good_data = [data for k, data in enumerate(self.data) if self.quality[k]]
        if n_last is not None:
            good_data = good_data[max(0, len(good_data) - n_last):]

        if good_data:
            return {
                'value': self._data_to_dict(good_data),
                'data_length': len(good_data)
            }

        else:
            return {
                'value': None,
                'data_length': 0
            }

    @staticmethod
    def _data_to_dict(data):
        """general method to transform data to metric"""
        raise NotImplementedError

    def append(self, response):
        """general method to save data"""
        raise NotImplementedError


class Availability(MetricBase):
    def __init__(self, logger, data_length):
        """Availability metric

        :param logger:
        :param data_length:
        """
        super().__init__(logger, data_length)

    @staticmethod
    def _data_to_dict(data):
        return 100 * sum(data) / len(data)

    def append(self, response):
        try:
            self._update_data(response.ok, quality = True)
        except AttributeError as e:
            self._update_data(False, quality = True)


class ResponseTime(MetricBase):
    def __init__(self, logger, data_length):
        """Response time metric

        :param logger:
        :param data_length:
        """
        super().__init__(logger, data_length)

    @staticmethod
    def _data_to_dict(data):
        raise NotImplementedError

    def append(self, response):
        try:
            self._update_data(response.elapsed.total_seconds(), quality = True)
        except AttributeError as e:
            self._update_data(None, quality = False)


class MaxResponseTime(ResponseTime):
    def __init__(self, logger, data_length):
        """max of response time metric

        :param logger:
        :param data_length:
        """
        super().__init__(logger, data_length)

    @staticmethod
    def _data_to_dict(data):
        return round(max(data) * 1000, 2)


class AvgResponseTime(ResponseTime):
    def __init__(self, logger, data_length):
        """average of response time metric

        :param logger:
        :param data_length:
        """
        super().__init__(logger, data_length)

    @staticmethod
    def _data_to_dict(data):
        return round(sum(data) / len(data) * 1000, 2)


class CountStatusCode(MetricBase):
    def __init__(self, logger, data_length):
        """count the number of status from the website, metric

        :param logger:
        :param data_length:
        """
        super().__init__(logger, data_length)

    def append(self, response):
        try:
            response_status_cat = response.status_code // 100
            self._update_data(response_status_cat, quality = True)
        except AttributeError as e:
            self._update_data(5, quality = True)

    @staticmethod
    def _data_to_dict(data):
        status_dict = {}
        for status in data:
            try:
                status_dict[f'{status}XX'] += 1
            except KeyError:
                status_dict[f'{status}XX'] = 1
        return status_dict
