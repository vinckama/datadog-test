#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Vincent Roy
@author: Vincent Roy
"""
import time
import requests
from .metrics import Availability, MaxResponseTime, AvgResponseTime, CountStatusCode
from threading import Thread, Event
import socket
from .alert import AvailabilityAlert


class WebsiteCheck(Thread):
    METRICS = {
        'availability': Availability,
        'max_response_time': MaxResponseTime,
        'average_response_time': AvgResponseTime,
        'response_codes_count': CountStatusCode,
    }

    def __init__(self, log, config):
        """Agent to check website

        This agent (thread) monitor the availability and the performance of the website
        :param log: a logger formatter from logging library
        :param config: dict with all config parameters
            name: the name of the website (mandatory)
            url: the url of the website (mandatory)
            term: number of seconds between two check
            memory_time: duration of the data stored about the website
            metrics: list of metrics, availability is set always by default
            availability_threshold: threshold for the availability alert
        """
        super().__init__()
        self.log = log

        try:
            self.name = config['name']
        except KeyError as e:
            self.log.error(f"{self.name} WEBSITE has not name, required attribute")
            raise e
        try:
            self.url = config['url']
        except KeyError as e:
            self.log.error(f"{self.name} WEBSITE has not url, required attribute")
            raise e
        self.term = float(config.get('term', 1))
        self.memory_time = int(config.get('memory_time', 60 * 90))

        self._metrics = self._format_metrics(config.get('metrics', []))
        self.alert = AvailabilityAlert(
            log = self.log,
            url = self.url,
            metric = self._metrics['availability'],
            threshold = int(config.get('availability_threshold', 80)),
            duration = 2 * 60,
            term = self.term,
        )
        self.shutdown_flag = Event()
        self.log.info(f"{self.name}'s website-check has been created")
        self.start()

    def check(self):
        """routine to check the availability of the website

        check the status of the website according to metrics
        :return:
        """
        start = time.time()
        r = None
        try:
            r = requests.get(self.url)
        except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.MissingSchema) \
                as e:
            length = int((time.time() - start) * 1000)
            self.log.debug(f"{self.url} is DOWN, error: {e}, Connection failed after {length} ms")
        except Exception as e:
            length = int((time.time() - start) * 1000)
            self.log.error(f"Unhandled exception {e}. Connection failed after {length} ms")
            raise
        for metric in self._metrics.values():
            metric.append(r)

    def _format_metrics(self, metric_list):
        """Map a list of metrics to a dict with metric instances

        :param metric_list: a list of metric
        :return: a dict of metrics with their names as keys
        """
        data_length = int(self.memory_time / self.term)
        formatted_metrics = {'availability': Availability(self.log, data_length)}
        for metric in metric_list:
            try:
                metric = metric.lower()
            except AttributeError:
                metric = ''

            try:
                formatted_metrics[metric] = self.METRICS[metric](self.log, data_length)
            except KeyError:
                self.log.warning(f'{metric} is not a defined metric {list(self.METRICS.keys())}, metric ignored')

        if not formatted_metrics:  # by default, check the availability
            formatted_metrics.append(self.METRICS['availability'])

        return formatted_metrics

    def run(self):
        """run thread method, overwritten

        thread routine with the check of the website (metrics) and the associated alerts
        :return:
        """
        while not self.shutdown_flag.is_set():
            start = time.time()
            self.check()
            self.alert.check()
            end = time.time()
            time.sleep(max(0, self.term - (end - start)))

    def __call__(self, **kwargs):
        """give info on website

        create and return a dict with all information on the website, the term, the length of data from metrics,
        the status of metrics, the status of alerts
        :param kwargs:
        :return:
        """
        duration = kwargs.get('duration', None)
        n_last = kwargs.get('n_last', None)
        if duration is not None:
            n_last = int(duration / self.term)

        metric_dict = {name: metric(n_last) for name, metric in self._metrics.items()}
        info_dict = {}
        info_dict['term'] = self.term
        info_dict['data_length'] = metric_dict['availability']['data_length']
        info_dict['url'] = self.url

        info_dict['metrics'] = {}
        for name, metric in metric_dict.items():
            info_dict['metrics'][name] = metric['value']

        info_dict['alert'] = self.alert()

        return info_dict

    @property
    def metrics(self):
        """getter function to give metrics names"""
        return list(self._metrics.keys())

    @metrics.setter
    def metrics(self, metric_list):
        """setter function to set metrics

        :param metric_list: list of metric names
        :return:
        """
        self._metrics = self._format_metrics(metric_list)
