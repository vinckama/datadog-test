#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Vincent Roy
@author: Vincent Roy
"""
import pytest
from website_check.alert import AvailabilityAlert
import logging
from website_check import metrics


class TestAvailabilityAlert:
    logger = logging.getLogger(__name__, )
    logger.setLevel('INFO')

    class Response:
        def __init__(self, ok):
            self.ok = ok

    @pytest.fixture()
    def metric(self):
        yield metrics.Availability(self.logger, 10)

    @pytest.fixture()
    def alert(self, metric):
        yield AvailabilityAlert(
            log = self.logger,
            url = 'test.url',
            metric = metric,
            threshold = 80,
            duration = 2,
            term = 1)

    def test_init(self, metric):
        # warning if duration < term
        alert = AvailabilityAlert(
            log = self.logger,
            url = 'test.url',
            metric = metric,
            threshold = 80,
            duration = 1,
            term = 2)
        assert alert.n_last == 1

        alert = AvailabilityAlert(
            log = self.logger,
            url = 'test.url',
            metric = metric,
            threshold = 80,
            duration = 4,
            term = 2)
        assert alert.n_last == 2

    def test_alert(self, caplog, alert):
        alert.alert(10)
        assert alert.state
        caplog.clear()

    def test_recovery(self, caplog, alert):
        alert.recovery(10)
        assert not alert.state
        caplog.clear()

    def test_check(self, caplog, alert, metric):
        metric.append(self.Response(True))
        assert alert.check() is None
        metric.append(self.Response(True))
        assert 100 == alert.check()

        metric.append(self.Response(False))
        assert 50 == alert.check()
        assert alert.state
        caplog.clear()

        metric.append(self.Response(True))
        metric.append(self.Response(True))
        assert 100 == alert.check()
        assert not alert.state
        caplog.clear()

    def test_call(self, alert, metric):
        assert isinstance(alert(), dict)

        metric.append(self.Response(False))
        info_dict = alert()
        assert 'availability' in info_dict.keys()
        assert 'status' in info_dict.keys()
        assert 'data_length' in info_dict.keys()
        assert 'status_time' in info_dict.keys()
        assert info_dict['availability'] is None

        metric.append(self.Response(True))
        info_dict = alert()
        assert info_dict['availability'] == 50
        assert info_dict['status']
        assert info_dict['data_length'] == 2
        assert isinstance(info_dict['status_time'], str)

