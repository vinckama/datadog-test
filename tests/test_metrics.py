#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Vincent Roy
@author: Vincent Roy
"""

import pytest
from website_check import metrics
from website_check import log


class TestMetricBase:
    """ test of the MetricBase class"""
    def test_init(self):
        logger = log.init_logging()
        with pytest.raises(TypeError) as e_info:
            metrics.MetricBase()
        with pytest.raises(ValueError) as e_info:
            metrics.MetricBase(logger, '1à')
            metrics.MetricBase(logger, '0')

    def test__update_data(self):
        logger = log.init_logging()
        base_metric = metrics.MetricBase(logger, 3)
        base_metric._update_data(0, True)
        base_metric._update_data(0, True)
        base_metric._update_data(0, True)
        assert len(base_metric.data) == 3

        base_metric._update_data(0, True)
        assert len(base_metric.data) == 3

    def test__call(self):
        logger = log.init_logging()
        base_metric = metrics.MetricBase(logger, 3)
        base_metric._update_data(0, True)
        base_metric._update_data(1, True)
        base_metric._update_data(2, False)
        with pytest.raises(NotImplementedError) as e_info:
            base_metric()
            base_metric(n_last = 1)

        base_metric = metrics.MetricBase(logger, 3)
        output = base_metric()
        assert output['data_length'] == 0
        assert output['value'] is None

        base_metric = metrics.MetricBase(logger, 3)
        base_metric._update_data(1, False)
        base_metric._update_data(2, False)
        output = base_metric()
        assert output['data_length'] == 0
        assert output['value'] is None


class TestAvailability:
    """ test of the metric Availability"""
    class Response:
        def __init__(self, ok):
            self.ok = ok

    def test_append(self):
        logger = log.init_logging()
        availability = metrics.Availability(logger, 3)
        availability.append('')
        assert availability.data == [False]

        availability = metrics.Availability(logger, 3)
        response = self.Response(ok = False)
        availability.append(response)
        assert availability.data == [False]

        availability = metrics.Availability(logger, 3)
        response = self.Response(ok = True)
        availability.append(response)
        assert availability.data == [True]

    def test__data_to_dict(self):
        logger = log.init_logging()
        availability = metrics.Availability(logger, 3)
        response = self.Response(ok = False)
        availability.append(response)
        response = self.Response(ok = True)
        availability.append(response)
        assert availability._data_to_dict(availability.data) == 50.0

#..
