#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Vincent Roy
@author: Vincent Roy
"""
import pytest
from website_check import utils


def test_message_duration():
    assert '1s' == utils.message_duration(1)
    assert '59s' == utils.message_duration(59)
    assert 'min' == utils.message_duration(60)
    assert 'min' == utils.message_duration(90)
    assert '3 min' == utils.message_duration(180)
    assert '59 min' == utils.message_duration(3599)
    assert 'hour' == utils.message_duration(3600)
    assert '2 h' == utils.message_duration(7200)


def test_recur_dict_message():
    assert [] == utils.recur_dict_message({})
    assert ['hello                             1\n'] == utils.recur_dict_message({'hello': 1})
    assert ['hello 1000\n'] == utils.recur_dict_message({'hello': 1000}, max_len = 0)
    assert ['hello                             1\n',
            'hello_2\n',
            '        world                     2\n'] == \
           utils.recur_dict_message({
               'hello': 1.0,
               'hello_2': {
                   'world': 2,
               }
           })


