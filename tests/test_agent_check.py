#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Vincent Roy
@author: Vincent Roy
"""
import pytest
from website_check import metrics
from website_check import log
from website_check import agent_check
import time

class TestWebsiteCheck:
    logger = log.init_logging()
    config = {
        'name': 'test',
        'url': 'test.url'
    }

    @pytest.fixture()
    def web_check(self):
        yield agent_check.WebsiteCheck(self.logger, self.config)

    def test_init(self, web_check):
        config = {
            'name': 'test',
        }
        with pytest.raises(KeyError):
            agent_check.WebsiteCheck(self.logger, config)
        config = {
            'url': 'test.url'
        }
        with pytest.raises(KeyError):
            agent_check.WebsiteCheck(self.logger, config)

        web_check.shutdown_flag.set()
        time.sleep(1)
        assert not web_check.is_alive()

    def test_check(self, web_check):
        web_check.shutdown_flag.set()
        n_data_metric = len(web_check._metrics['availability'].data)
        web_check.check()
        # TODO change this weird test
        assert len(web_check._metrics['availability'].data) - n_data_metric == 1

    def test__format_metrics(self, web_check):
        assert list(web_check._format_metrics([]).keys()) == ['availability']
        assert list(web_check._format_metrics(['availability', 'max_response_time']).keys()) == \
               ['availability', 'max_response_time']
        assert list(web_check._format_metrics(['max_response_time']).keys()) == ['availability', 'max_response_time']
        assert list(web_check._format_metrics(['adazdaz']).keys()) == ['availability']
        web_check.shutdown_flag.set()

    def test_call(self, web_check):
        check_info = web_check()
        web_check.shutdown_flag.set()

        assert isinstance(check_info, dict)
        assert 'term' in check_info.keys()
        assert 'data_length' in check_info.keys()
        assert 'url' in check_info.keys()
        assert 'metrics' in check_info.keys()
        assert 'alert' in check_info.keys()

        assert list(check_info['metrics'].keys()) == web_check.metrics


    def test_metrics(self, web_check):
        assert web_check.metrics == ['availability']

        web_check.metrics = ['availability', 'max_response_time']
        assert web_check.metrics == ['availability', 'max_response_time']
        web_check.shutdown_flag.set()

