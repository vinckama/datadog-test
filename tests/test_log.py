#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Vincent Roy
@author: Vincent Roy
"""
import pytest
from website_check import log
import logging
import random
import importlib


def test_call_one():
    @log.call_one
    def rand():
        return random.random()
    assert random.random() != random.random()
    assert rand() == rand()


def test__get_loglevel():
    importlib.reload(log)
    assert log._get_loglevel('CRITICAL') == logging.CRITICAL
    importlib.reload(log)
    assert log._get_loglevel('critical') == logging.CRITICAL
    importlib.reload(log)
    assert log._get_loglevel('') == logging.INFO
    importlib.reload(log)
    assert log._get_loglevel('aze zadz') == logging.INFO


def test_init_logging():
    importlib.reload(log)
    assert log.init_logging().level == 20
    importlib.reload(log)
    assert log.init_logging('critical').level == 50
    importlib.reload(log)
    assert log.init_logging('aezaeza').level == 20






