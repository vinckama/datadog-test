#!/bin/bash
#(C) Vincent Roy
#@author: Vincent Roy

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
python3  "${DIR}"/setup.py install --user

row=$(tput lines)
resize -s ${row} 120
for i in $(seq 1 ${row})
do
    echo -e ""
done

python3 "${DIR}"/app.py
