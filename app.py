#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Vincent Roy
@author: Vincent Roy
"""

import website_check
import argparse
import sys

parser = argparse.ArgumentParser(
    description = 'Data Acquisition manager')
parser.add_argument('--config', help = 'config file folder', default='')
parser.add_argument('--loglevel',
                    help = 'level of logs, info by default',
                    choices=['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG'],
                    default = 'INFO')
args = parser.parse_args(sys.argv[1:])
website_check.Manager(config= args.config, loglevel = args.loglevel)
