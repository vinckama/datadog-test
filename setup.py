#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Vincent Roy
@author: Vincent Roy
"""
from setuptools import setup, find_packages
from os import path
HERE = path.abspath(path.dirname(__file__))

with open(path.join(HERE, 'README.md'), 'r', encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='website_check',
    version='1.0',
    description='check website agent for DataDog project',
    author='Vincent Roy',
    author_email='vincent.roy@student.ecp.fr',
    long_description=long_description,
    long_description_content_type = "text/markdown",
    url="https://gitlab.com/vinckama/datadog-test",
    packages = find_packages("website_check", exclude=["*test*"]),
    include_package_data=True,
    install_requires=[
        'pyyaml',
        'requests>=2.23.0'
    ],
    python_requires='>=3.7'
)
